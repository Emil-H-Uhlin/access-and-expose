INSERT INTO franchise (name, description) VALUES ('Harry Potter',
                                                  'Film series based on the ' ||
                                                  'eponymous novels by J.K. Rowling.');

INSERT INTO franchise (name) VALUES ('Resident Evil');
INSERT INTO franchise (name) VALUES ('DC Comics');


INSERT INTO movie (title, genre, year, franchise_id, director)              -- id 1
VALUES ('Harry Potter and the Philosopher''s Stone', 'Fantasy, Adventure',
        2001, 1, 'Chris Columbus');

INSERT INTO movie (title, genre, year, franchise_id, director)              -- id 2
VALUES ('Fantastic Beasts and Where to Find Them', 'Fantasy, Adventure',
        2016, 1, 'David Yates');

INSERT INTO movie (title, genre, year, franchise_id, director)              -- id 3
VALUES ('Resident Evil', 'Action, Horror', 2002, 2, 'Paul W.S. Anderson');

INSERT INTO movie (title, genre, year, franchise_id, director)              -- id 4
VALUES ('Resident Evil: Welcome to Raccoon City', 'Action, Horror, Sci-Fi',
        2021, 2, 'Johannes Roberts');

INSERT INTO movie (title, genre, year, franchise_id, director)
VALUES ('The Batman', 'Action', 2021, 3, 'Matt Reeves');                    -- id 5


INSERT INTO character (name, actor, gender)
VALUES ('Harry Potter', 'Daniel Radcliffe', 'Male');                        -- id 1
INSERT INTO character (name, actor, gender)
VALUES ('Alice', 'Milla Jovovich', 'Female');                               -- id 2
INSERT INTO character (name, actor, gender)
VALUES ('Chris Redfield', 'Robbie Amell', 'Male');                          -- id 3
INSERT INTO character (name, alias, actor, gender)
VALUES ('Bruce Wayne', 'The Batman', 'Robert Pattinson', 'Male');                -- id 4

INSERT INTO character_movie (movie_id, character_id)
VALUES (1, 1);

INSERT INTO character_movie (movie_id, character_id)
VALUES (3, 2);

INSERT INTO character_movie (movie_id, character_id)
VALUES (4, 3);

INSERT INTO character_movie (movie_id, character_id)
VALUES (5, 4)