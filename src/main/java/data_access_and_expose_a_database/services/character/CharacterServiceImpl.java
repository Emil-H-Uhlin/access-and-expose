package data_access_and_expose_a_database.services.character;

import data_access_and_expose_a_database.models.Character;
import data_access_and_expose_a_database.repositories.CharacterRepository;
import data_access_and_expose_a_database.services.exceptions.CharacterNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class CharacterServiceImpl implements CharacterService {
	private final CharacterRepository characterRepository;
	
	public CharacterServiceImpl(CharacterRepository characterRepository) {
		this.characterRepository = characterRepository;
	}
	
	@Override
	public Character findById(Integer id) {
		return characterRepository.findById(id)
				       .orElseThrow(() -> new CharacterNotFoundException(id));
	}
	
	@Override
	public Collection<Character> findAll() {
		return characterRepository.findAll();
	}
	
	@Override
	public Character add(Character entity) {
		return characterRepository.save(entity);
	}
	
	@Override
	public Character update(Character entity) {
		return characterRepository.save(entity);
	}
	
	@Override
	public void deleteById(Integer id) {
		characterRepository.deleteById(id);
	}
	
	@Override
	public Collection<Character> findAllByMovieId(int id) {
		return characterRepository.findAllByMovieId(id);
	}
	
	@Override
	public Collection<Character> findAllByFranchiseId(int id) {
		return characterRepository.findAllByFranchiseId(id);
	}
}
