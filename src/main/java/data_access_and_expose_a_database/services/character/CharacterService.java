package data_access_and_expose_a_database.services.character;

import data_access_and_expose_a_database.models.Character;
import data_access_and_expose_a_database.services.CrudService;

import java.util.Collection;

public interface CharacterService extends CrudService<Character, Integer> {
	Collection<Character> findAllByMovieId(int id);
	Collection<Character> findAllByFranchiseId(int id);
}
