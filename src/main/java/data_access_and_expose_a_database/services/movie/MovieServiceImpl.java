package data_access_and_expose_a_database.services.movie;

import data_access_and_expose_a_database.models.Movie;
import data_access_and_expose_a_database.repositories.MovieRepository;
import data_access_and_expose_a_database.services.exceptions.MovieNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class MovieServiceImpl implements MovieService {
	private final MovieRepository movieRepository;
	
	public MovieServiceImpl(MovieRepository movieRepository) {
		this.movieRepository = movieRepository;
	}
	
	@Override
	public Movie findById(Integer id) {
		return movieRepository.findById(id)
				       .orElseThrow(() -> new MovieNotFoundException(id));
	}
	
	@Override
	public Collection<Movie> findAll() {
		return movieRepository.findAll();
	}
	
	@Override
	public Movie add(Movie entity) {
		return movieRepository.save(entity);
	}
	
	@Override
	public Movie update(Movie entity) {
		return movieRepository.save(entity);
	}
	
	@Override
	public void deleteById(Integer id) {
		movieRepository.deleteById(id);
	}
	
	@Override
	public Collection<Movie> findAllByFranchiseId(int id) {
		return movieRepository.findAllByFranchiseId(id);
	}
}
