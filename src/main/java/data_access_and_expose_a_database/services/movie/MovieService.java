package data_access_and_expose_a_database.services.movie;

import data_access_and_expose_a_database.models.Movie;
import data_access_and_expose_a_database.services.CrudService;

import java.util.Collection;

public interface MovieService extends CrudService<Movie, Integer> {
	Collection<Movie> findAllByFranchiseId(int id);
}
