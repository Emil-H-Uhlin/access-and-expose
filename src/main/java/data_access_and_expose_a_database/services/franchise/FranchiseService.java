package data_access_and_expose_a_database.services.franchise;

import data_access_and_expose_a_database.models.Franchise;
import data_access_and_expose_a_database.services.CrudService;

public interface FranchiseService extends CrudService<Franchise, Integer> {
}
