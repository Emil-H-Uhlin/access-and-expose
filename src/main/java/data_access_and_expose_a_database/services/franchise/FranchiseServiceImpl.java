package data_access_and_expose_a_database.services.franchise;

import data_access_and_expose_a_database.models.Franchise;
import data_access_and_expose_a_database.repositories.FranchiseRepository;
import data_access_and_expose_a_database.services.exceptions.FranchiseNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class FranchiseServiceImpl implements FranchiseService {
	private final FranchiseRepository franchiseRepository;
	
	public FranchiseServiceImpl(FranchiseRepository franchiseRepository) {
		this.franchiseRepository = franchiseRepository;
	}
	
	@Override
	public Franchise findById(Integer id) {
		return franchiseRepository.findById(id)
				       .orElseThrow(() -> new FranchiseNotFoundException(id));
	}
	
	@Override
	public Collection<Franchise> findAll() {
		return franchiseRepository.findAll();
	}
	
	@Override
	public Franchise add(Franchise entity) {
		return franchiseRepository.save(entity);
	}
	
	@Override
	public Franchise update(Franchise entity) {
		return franchiseRepository.save(entity);
	}
	
	@Override
	public void deleteById(Integer id) {
		franchiseRepository.deleteById(id);
	}
}
