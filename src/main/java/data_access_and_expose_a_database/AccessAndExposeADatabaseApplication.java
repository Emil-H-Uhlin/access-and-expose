package data_access_and_expose_a_database;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AccessAndExposeADatabaseApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(AccessAndExposeADatabaseApplication.class, args);
	}
	
}
