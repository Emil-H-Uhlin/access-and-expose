package data_access_and_expose_a_database.controllers;

import data_access_and_expose_a_database.mappers.CharacterMapper;
import data_access_and_expose_a_database.mappers.FranchiseMapper;
import data_access_and_expose_a_database.mappers.MovieMapper;
import data_access_and_expose_a_database.models.dto.character.CharacterDTO;
import data_access_and_expose_a_database.models.dto.movie.AddMovieDTO;
import data_access_and_expose_a_database.models.dto.movie.MovieDTO;
import data_access_and_expose_a_database.models.requestbodies.UpdateCharactersBody;
import data_access_and_expose_a_database.services.character.CharacterService;
import data_access_and_expose_a_database.services.movie.MovieService;
import data_access_and_expose_a_database.util.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Arrays;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "api/v1/movies")
public class MovieController {
	private final MovieService movieService;
	private final CharacterService characterService;
	
	private final MovieMapper movieMapper;
	private final CharacterMapper characterMapper;
	
	@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
	public MovieController(MovieService movieService, MovieMapper movieMapper,
	                       CharacterMapper characterMapper, FranchiseMapper franchiseMapper, CharacterService characterService) {
		
		this.movieService = movieService;
		this.movieMapper = movieMapper;
		this.characterMapper = characterMapper;
		this.characterService = characterService;
	}
	
	@Operation(summary = "Get all movies")
	@ApiResponse(responseCode = "200",
			description = "OK",
			content = @Content(mediaType = "application/json",
					schema = @Schema(implementation = MovieDTO.class)
			)
	)
	@GetMapping
	public ResponseEntity findAll() {
		return ResponseEntity.ok(
				movieMapper.movieToMovieDto(movieService.findAll())
		);
	}
	
	@Operation(summary = "Find movies by franchise ID")
	@ApiResponse(responseCode = "200",
			description = "OK",
			content = @Content(mediaType = "application/json",
					schema = @Schema(implementation = MovieDTO.class)
			)
	)
	@GetMapping("/franchiseId={id}")
	public ResponseEntity findByFranchiseId(@PathVariable int id) {
		return ResponseEntity.ok(
				movieMapper.movieToMovieByFranchiseDto(movieService.findAllByFranchiseId(id))
		);
	}
	
	@Operation(summary = "Find movie by ID")
	@ApiResponses({
			@ApiResponse(responseCode = "200",
					description = "OK",
					content = @Content(mediaType = "application/json",
							schema = @Schema(implementation = MovieDTO.class)
					)
			),
			@ApiResponse(responseCode = "404",
					description = "No movie with supplied ID",
					content = @Content(mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			)
	})
	@GetMapping("/{id}")
	public ResponseEntity findById(@PathVariable int id) {
		return ResponseEntity.ok(
				movieMapper.movieToMovieDto(movieService.findById(id))
		);
	}
	
	@Operation(summary = "Add movie to DB")
	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping
	public ResponseEntity addMovie(@RequestBody AddMovieDTO dto) {
		var addedCharacter = movieService.add(
				movieMapper.movieAddDtoToMovie(dto)
		);
		
		URI uri = URI.create("characters/" + addedCharacter.getId());
		
		return ResponseEntity.created(uri).build();
	}
	
	@Operation(summary = "Update movie with ID")
	@ApiResponses({
			@ApiResponse(responseCode = "400",
					description = "Mismatching ID in supplied body"),
			@ApiResponse(responseCode = "204", description = "OK")
	})
	@PutMapping("/{id}")
	public ResponseEntity updateMovie(@RequestBody MovieDTO dto, @PathVariable int id) {
		if (dto.getId() != id)
			return ResponseEntity.badRequest().build();
		
		movieService.update(
				movieMapper.movieDtoToMovie(dto)
		);
		
		return ResponseEntity.noContent().build();
	}
	
	@Operation(summary = "Delete movie entry by ID")
	@DeleteMapping("/{id}")
	public ResponseEntity deleteById(@PathVariable int id) {
		movieService.deleteById(id);
		return ResponseEntity.noContent().build();
	}
	
	@Operation(summary = "Get characters in movie with ID")
	@ApiResponses({
			@ApiResponse(responseCode = "404",
					description = "There is no movie with supplied ID",
					content = @Content(mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			),
			@ApiResponse(responseCode = "200",
					description = "OK",
					content = @Content(mediaType = "application/json",
							schema = @Schema(implementation = CharacterDTO.class)
					)
			)
	})
	@GetMapping("/{id}/characters")
	public ResponseEntity getCharactersOfMovieWithId(@PathVariable int id) {
		return ResponseEntity.ok(
				characterMapper.characterToCharacterByMovieDto(movieService.findById(id).getCharacters())
		);
	}
	
	@Operation(summary = "Update characters of movie with ID")
	@ApiResponses({
			@ApiResponse(responseCode = "404",
					description = "There is no movie with supplied ID",
					content = @Content(mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			),
			@ApiResponse(responseCode = "204", description = "OK")
	})
	@PutMapping("/{id}/characters")
	public ResponseEntity updateCharactersOfMovieWithId(@PathVariable int id, @RequestBody UpdateCharactersBody body) {
		var movie = movieService.findById(id);
		movie.setCharacters(Arrays.stream(body.getCharacterIds())
				                    .map(characterService::findById)
				                    .collect(Collectors.toSet()));
		
		movieService.update(movie);
		
		return ResponseEntity.noContent().build();
	}
}
