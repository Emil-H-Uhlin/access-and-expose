package data_access_and_expose_a_database.controllers;

import data_access_and_expose_a_database.mappers.FranchiseMapper;
import data_access_and_expose_a_database.mappers.MovieMapper;
import data_access_and_expose_a_database.models.dto.franchise.AddFranchiseDTO;
import data_access_and_expose_a_database.models.dto.franchise.FranchiseDTO;
import data_access_and_expose_a_database.models.dto.movie.MovieDTO;
import data_access_and_expose_a_database.models.requestbodies.UpdateMoviesBody;
import data_access_and_expose_a_database.services.franchise.FranchiseService;
import data_access_and_expose_a_database.services.movie.MovieService;
import data_access_and_expose_a_database.util.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Arrays;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "api/v1/franchises")
public class FranchiseController {
	private final FranchiseService franchiseService;
	private final MovieService movieService;
	
	private final FranchiseMapper franchiseMapper;
	private final MovieMapper movieMapper;
	
	@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
	public FranchiseController(FranchiseService franchiseService, MovieService movieService,
	                           FranchiseMapper franchiseMapper, MovieMapper movieMapper) {
		
		this.franchiseService = franchiseService;
		this.movieService = movieService;
		this.franchiseMapper = franchiseMapper;
		this.movieMapper = movieMapper;
	}
	
	@Operation(summary = "Get all franchises")
	@ApiResponse(responseCode = "200",
			description = "OK",
			content = @Content(mediaType = "application/json",
					schema = @Schema(implementation = FranchiseDTO.class)
			)
	)
	@GetMapping
	public ResponseEntity findAll() {
		return ResponseEntity.ok(
				franchiseMapper.franchiseToFranchiseDto(franchiseService.findAll())
		);
	}
	
	@Operation(summary = "Get franchise by ID")
	@ApiResponses({
			@ApiResponse(responseCode = "200",
					description = "OK",
					content = {
					@Content(mediaType = "application/json",
							schema = @Schema(implementation = FranchiseDTO.class)
					)
			}),
			@ApiResponse(responseCode = "404",
			description = "There is no franchise with supplied ID",
			content = {
					@Content(mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			})
	})
	@GetMapping("/{id}")
	public ResponseEntity findById(@PathVariable int id) {
		return ResponseEntity.ok(
				franchiseMapper.franchiseToFranchiseDto(franchiseService.findById(id))
		);
	}
	
	@Operation(summary = "Insert a new franchise to DB")
	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping
	public ResponseEntity addFranchise(@RequestBody AddFranchiseDTO dto) {
		var addedCharacter = franchiseService.add(
				franchiseMapper.franchiseAddDtoToFranchise(dto)
		);
		
		URI uri = URI.create("characters/" + addedCharacter.getId());
		
		return ResponseEntity.created(uri).build();
	}
	
	@Operation(summary = "Update franchise with id")
	@ApiResponses({
			@ApiResponse(responseCode = "400", description = "Mismatching ID in supplied body"),
			@ApiResponse(responseCode = "204", description = "OK")
	})
	@PutMapping("/{id}")
	public ResponseEntity updateFranchise(@RequestBody FranchiseDTO dto, @PathVariable int id) {
		if (dto.getId() != id)
			return ResponseEntity.badRequest().build();
		
		franchiseService.update(
				franchiseMapper.franchiseDtoToFranchise(dto)
		);
		
		return ResponseEntity.noContent().build();
	}
	
	@Operation(summary = "Delete franchise entry by ID")
	@DeleteMapping("/{id}")
	public ResponseEntity deleteById(@PathVariable int id) {
		franchiseService.deleteById(id);
		return ResponseEntity.noContent().build();
	}
	
	@Operation(summary = "Get movies of franchise with ID")
	@ApiResponses({
			@ApiResponse(responseCode = "404",
			description = "There is no franchise with supplied ID",
			content = {
					@Content(mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)),
			}),
			@ApiResponse(responseCode = "200",
					description = "OK",
					content = {
					@Content(mediaType = "application/json",
							schema = @Schema(implementation = MovieDTO.class))
			})
	})
	@GetMapping("/{id}/movies")
	public ResponseEntity getMoviesOfFranchiseWithId(@PathVariable int id) {
		return ResponseEntity.ok(
				movieMapper.movieToMovieByFranchiseDto(franchiseService.findById(id).getMovies())
		);
	}
	
	@Operation(summary = "Update movies of franchise with ID")
	@ApiResponses({
			@ApiResponse(responseCode = "404",
					description = "There is no franchise with supplied ID",
					content = {
					@Content(mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class))
			}),
			@ApiResponse(responseCode = "204", description = "Updated movies")
	})
	@PutMapping("/{id}/movies")
	public ResponseEntity updateMoviesOfFranchiseWithId(@PathVariable int id, @RequestBody UpdateMoviesBody body) {
		var franchise = franchiseService.findById(id);
		franchise.setMovies(
				Arrays.stream(body.getMovieIds())
						.map(movieService::findById)
						.collect(Collectors.toSet()));
		
		franchiseService.update(franchise);
		
		return ResponseEntity.noContent().build();
	}
}
