package data_access_and_expose_a_database.controllers;

import data_access_and_expose_a_database.mappers.CharacterMapper;
import data_access_and_expose_a_database.mappers.MovieMapper;
import data_access_and_expose_a_database.models.dto.character.AddCharacterDTO;
import data_access_and_expose_a_database.models.dto.character.CharacterByFranchiseDTO;
import data_access_and_expose_a_database.models.dto.character.CharacterByMovieDTO;
import data_access_and_expose_a_database.models.dto.character.CharacterDTO;
import data_access_and_expose_a_database.models.dto.movie.MovieDTO;
import data_access_and_expose_a_database.models.requestbodies.UpdateMoviesBody;
import data_access_and_expose_a_database.services.character.CharacterService;
import data_access_and_expose_a_database.services.movie.MovieService;
import data_access_and_expose_a_database.util.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Arrays;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "api/v1/characters")
public class CharacterController {
	private final CharacterService characterService;
	private final MovieService movieService;
	
	private final CharacterMapper characterMapper;
	private final MovieMapper movieMapper;
	
	@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
	public CharacterController(CharacterService characterService, MovieService movieService,
	                           CharacterMapper characterMapper, MovieMapper movieMapper) {
		
		this.characterService = characterService;
		this.movieService = movieService;
		this.characterMapper = characterMapper;
		this.movieMapper = movieMapper;
	}
	
	@Operation(summary = "Get all characters")
	@ApiResponse(responseCode = "200",
			description = "OK",
			content = @Content(mediaType = "application/json",
					schema = @Schema(implementation = CharacterDTO.class)
			)
	)
	@GetMapping
	public ResponseEntity findAll() {
		return ResponseEntity.ok(
				characterMapper.characterToCharacterDto(characterService.findAll())
		);
	}
	
	@Operation(summary = "Find characters in movie with given ID")
	@ApiResponses({
			@ApiResponse(responseCode = "404",
					description = "Error in movies - no movie with supplied ID(s)",
					content = @Content(mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)
					)
			),
			@ApiResponse(responseCode = "200",
					description = "OK",
					content = @Content(mediaType = "application/json",
							schema = @Schema(implementation = CharacterByMovieDTO.class)
					)
			)
	})
	@GetMapping("/movieId={id}")
	public ResponseEntity findByMovieId(@PathVariable int id) {
		return ResponseEntity.ok(
				characterMapper.characterToCharacterByMovieDto(characterService.findAllByMovieId(id))
		);
	}
	
	@Operation(summary = "Find characters in franchise with given ID")
	@ApiResponse(responseCode = "200",
			description = "OK",
			content = @Content(mediaType = "application/json",
					schema = @Schema(implementation = CharacterByFranchiseDTO.class)
			)
	)
	@GetMapping("/franchiseId={id}")
	public ResponseEntity findByFranchiseId(@PathVariable int id) {
		return ResponseEntity.ok(
				characterMapper.characterToCharacterByFranchiseDto(characterService.findAllByFranchiseId(id))
		);
	}
	
	@Operation(summary = "Get a character by id")
	@ApiResponses({
			@ApiResponse(responseCode = "200",
					description = "OK",
					content =  @Content(mediaType = "application/json",
							schema = @Schema(implementation = CharacterDTO.class))
			),
			@ApiResponse(responseCode = "404",
					description = "There is no character with supplied ID",
					content = @Content(mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class)))
	})
	@GetMapping("/{id}")
	public ResponseEntity findById(@PathVariable int id) {
		return ResponseEntity.ok(
				characterMapper.characterToCharacterDto(characterService.findById(id))
		);
	}
	
	@Operation(summary = "Insert new character to DB")
	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping
	public ResponseEntity addCharacter(@RequestBody AddCharacterDTO dto) {
		var addedCharacter = characterService.add(characterMapper.addDtoToCharacter(dto));
		
		URI uri = URI.create("characters/" + addedCharacter.getId());
		
		return ResponseEntity.created(uri).build();
	}
	
	@Operation(summary = "Update character with ID")
	@ApiResponses({
			@ApiResponse(responseCode = "400",
					description = "Mismatching ID in supplied body"),
			@ApiResponse(responseCode = "204", description = "OK")
	})
	@PutMapping("/{id}")
	public ResponseEntity updateCharacter(@RequestBody CharacterDTO entity, @PathVariable int id) {
		if (entity.getId() != id)
			return ResponseEntity.badRequest().build();
		
		characterService.update(characterMapper.characterDtoToCharacter(entity));
		return ResponseEntity.noContent().build();
	}
	
	@Operation(summary = "Delete character entry by ID")
	@DeleteMapping("/{id}")
	public ResponseEntity deleteById(@PathVariable int id) {
		characterService.deleteById(id);
		return ResponseEntity.noContent().build();
	}
	
	@Operation(summary = "Get movies of character by ID")
	@ApiResponses({
			@ApiResponse(responseCode = "404",
					description = "There is no character with supplied ID",
					content = {
					@Content(mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class))
					}),
			@ApiResponse(responseCode = "200",
					description = "OK",
					content = {
					@Content(mediaType = "application/json",
							schema = @Schema(implementation = MovieDTO.class))
			})
	})
	@GetMapping("/{id}/movies")
	public ResponseEntity getMoviesOfCharacterWithId(@PathVariable int id) {
		return ResponseEntity.ok(
				movieMapper.movieToMovieDto(characterService.findById(id).getMovies())
		);
	}
	
	@Operation(summary = "Update movies of character with ID")
	@ApiResponses({
			@ApiResponse(responseCode = "404",
					description = "There is no character with supplied ID",
					content = {
					@Content(mediaType = "application/json",
							schema = @Schema(implementation = ApiErrorResponse.class))
					}),
			@ApiResponse(responseCode = "204", description = "Updated movies")
	})
	@PutMapping("/{id}/movies")
	public ResponseEntity updateMoviesOfCharacterWithId(@PathVariable int id, @RequestBody UpdateMoviesBody body) {
		var character = characterService.findById(id);
		character.setMovies(
				Arrays.stream(body.getMovieIds())
						.map(movieService::findById)
						.collect(Collectors.toSet()));
		
		characterService.update(character);
		
		return ResponseEntity.noContent().build();
	}
}
