package data_access_and_expose_a_database.util;

import lombok.Getter;

@Getter
public class ApiErrorResponse {
	private String timeStamp;
	private Integer status;
	private String error;
	private String trace;
	private String message;
	private String path;
}
