package data_access_and_expose_a_database.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Movie {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id = -1;
	
	@Column(nullable = false)
	private String title;
	
	private String genre;
	
	@Column(name = "year")
	private int releaseYear;
	
	@Column(length = 80, nullable = false)
	private String director;
	
	@Column(name = "img_url")
	private String imageUrl;
	
	@Column(name = "trailer_url")
	private String trailerUrl;
	
	@ManyToOne
	@JoinColumn(name = "franchise_id")
	private Franchise franchise;
	
	@ManyToMany(mappedBy = "movies")
	private Set<Character> characters;
	
	public Movie(@NonNull String title, @NonNull String director) {
		this.title = title;
		this.director = director;
	}
}
