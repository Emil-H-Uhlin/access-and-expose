package data_access_and_expose_a_database.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Character {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id = -1;
	
	@Column(length = 80, nullable = false)
	private String name;
	
	@Column(length = 20, name = "alias")
	private String alias;
	
	@Column(length = 80, nullable = false, name = "actor")
	private String actorName;
	
	private String gender;
	
	@Column(name = "img_url")
	private String imageUrl;
	
	@ManyToMany
	@JoinTable(
			name="character_movie",
			joinColumns = { @JoinColumn(name = "character_id") },
			inverseJoinColumns = { @JoinColumn(name = "movie_id") }
	)
	private Set<Movie> movies;
	
	public Character(@NonNull String name, @NonNull String actorName) {
		this.name = name;
		this.actorName = actorName;
	}
}
