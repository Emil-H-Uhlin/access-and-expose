package data_access_and_expose_a_database.models.dto.franchise;

import lombok.Data;

import java.util.Set;

@Data
public class FranchiseDTO {
	private int id;
	private String name;
	private String description;
	private Set<Integer> movies;
}
