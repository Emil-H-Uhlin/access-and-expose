package data_access_and_expose_a_database.models.dto.franchise;

import lombok.Data;

@Data
public class AddFranchiseDTO {
	private String name;
	private String description;
}