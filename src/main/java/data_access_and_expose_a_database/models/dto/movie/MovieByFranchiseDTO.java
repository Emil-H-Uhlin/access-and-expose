package data_access_and_expose_a_database.models.dto.movie;

import lombok.Data;

import java.util.Set;

@Data
public class MovieByFranchiseDTO {
	private int id;
	private String title;
	private String director;
	private Set<Integer> characters;
}
