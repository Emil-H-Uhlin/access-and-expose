package data_access_and_expose_a_database.models.dto.movie;

import lombok.Data;

@Data
public class AddMovieDTO {
	private String title;
	private String genre;
	private String director;
	private int releaseYear;
	private int franchiseId;
}
