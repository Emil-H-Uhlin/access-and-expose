package data_access_and_expose_a_database.models.dto.movie;

import lombok.Data;

import java.util.Set;

@Data
public class MovieDTO {
	private int id;
	private String title;
	private String genre;
	private String director;
	private int releaseYear;
	private int franchiseId;
	private Set<Integer> characters;
}
