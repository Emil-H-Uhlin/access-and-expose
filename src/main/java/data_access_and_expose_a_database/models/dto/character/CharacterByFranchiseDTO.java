package data_access_and_expose_a_database.models.dto.character;

import lombok.Data;

import java.util.Set;

@Data
public class CharacterByFranchiseDTO {
	private int id;
	private String name;
	private String actorName;
	private Set<String> movies;
}
