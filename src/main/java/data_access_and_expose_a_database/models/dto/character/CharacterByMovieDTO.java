package data_access_and_expose_a_database.models.dto.character;

import lombok.Data;

@Data
public class CharacterByMovieDTO {
	private int id;
	private String name;
	private String alias;
	private String actorName;
}
