package data_access_and_expose_a_database.models.dto.character;

import lombok.Data;

import java.util.Set;

@Data
public class CharacterDTO {
	private int id;
	private String name;
	private String alias;
	private String actorName;
	private String gender;
	private Set<Integer> movies;
}
