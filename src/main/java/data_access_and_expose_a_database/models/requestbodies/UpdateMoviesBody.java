package data_access_and_expose_a_database.models.requestbodies;

import lombok.Getter;

@Getter
public class UpdateMoviesBody {
	Integer[] movieIds;
}
