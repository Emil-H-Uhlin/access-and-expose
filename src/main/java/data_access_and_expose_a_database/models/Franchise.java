package data_access_and_expose_a_database.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Franchise {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id = -1;
	
	@Column(nullable = false)
	private String name;
	
	private String description;
	
	@OneToMany(mappedBy = "franchise")
	private Set<Movie> movies;
	
	public Franchise(@NonNull String name) {
		this.name = name;
	}
}
