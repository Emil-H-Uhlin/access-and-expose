package data_access_and_expose_a_database.repositories;

import data_access_and_expose_a_database.models.Character;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface CharacterRepository extends JpaRepository<Character, Integer> {
	@Query(value = """
    SELECT character.* FROM character
        JOIN character_movie AS cm
        ON cm.character_id = character.id
            JOIN movie
            ON movie_id = movie.id
		WHERE movie_id = ?
""", nativeQuery = true)
	Set<Character> findAllByMovieId(int id);
	
	@Query(value = """
	SELECT character.* FROM character
        JOIN character_movie AS cm
        ON cm.character_id = character.id
            JOIN movie
            ON movie_id = movie.id
                JOIN franchise
                ON franchise_id = franchise.id
        WHERE franchise.id = ?
""", nativeQuery = true)
	Set<Character> findAllByFranchiseId(int id);
}
