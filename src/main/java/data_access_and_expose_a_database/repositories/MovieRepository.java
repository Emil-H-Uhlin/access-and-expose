package data_access_and_expose_a_database.repositories;

import data_access_and_expose_a_database.models.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Integer> {
	@Query(value = """
	SELECT movie.* FROM movie
		JOIN franchise
		ON movie.franchise_id = franchise.id
	WHERE franchise.id = ?
""",nativeQuery = true)
	Collection<Movie> findAllByFranchiseId(int id);
}
