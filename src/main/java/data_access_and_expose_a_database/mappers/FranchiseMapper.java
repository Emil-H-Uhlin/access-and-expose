package data_access_and_expose_a_database.mappers;

import data_access_and_expose_a_database.models.Franchise;
import data_access_and_expose_a_database.models.Movie;
import data_access_and_expose_a_database.models.dto.franchise.AddFranchiseDTO;
import data_access_and_expose_a_database.models.dto.franchise.FranchiseDTO;
import data_access_and_expose_a_database.services.movie.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring",
		uses = {
		MovieService.class
})
public abstract class FranchiseMapper {
	@Autowired
	protected MovieService movieService;
	
	@Mapping(target = "movies", source = "movies", qualifiedByName = "moviesToIds")
	public abstract FranchiseDTO franchiseToFranchiseDto(Franchise franchise);
	
	public abstract Collection<FranchiseDTO> franchiseToFranchiseDto(Collection<Franchise> franchises);
	
	@Mapping(target = "movies", source = "movies", qualifiedByName = "movieIdsToMovies")
	public abstract Franchise franchiseDtoToFranchise(FranchiseDTO dto);
	
	public abstract Collection<Franchise> franchiseDtoToFranchise(Collection<FranchiseDTO> dtos);
	
	public abstract Franchise franchiseAddDtoToFranchise(AddFranchiseDTO dto);
	
	@Named("moviesToIds")
	Set<Integer> mapMoviesToIds(Set<Movie> source) {
		if (source == null) return null;
		
		return source.stream().map(Movie::getId).collect(Collectors.toSet());
	}
	
	@Named("movieIdsToMovies")
	Set<Movie> mapIdsToMovies(Set<Integer> ids) {
		if (ids == null) return null;
		
		return ids.stream().map(id -> movieService.findById(id)).collect(Collectors.toSet());
	}
}
