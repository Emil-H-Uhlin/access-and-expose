package data_access_and_expose_a_database.mappers;

import data_access_and_expose_a_database.models.Franchise;
import data_access_and_expose_a_database.models.Movie;
import data_access_and_expose_a_database.models.Character;
import data_access_and_expose_a_database.models.dto.movie.AddMovieDTO;
import data_access_and_expose_a_database.models.dto.movie.MovieByFranchiseDTO;
import data_access_and_expose_a_database.models.dto.movie.MovieDTO;
import data_access_and_expose_a_database.services.character.CharacterService;
import data_access_and_expose_a_database.services.franchise.FranchiseService;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring",
uses = {
		FranchiseService.class,
		CharacterService.class
})
public abstract class MovieMapper {
	@Autowired
	private FranchiseService franchiseService;
	
	@Autowired
	private CharacterService characterService;
	
	@Mappings({
			@Mapping(target = "franchiseId", source = "franchise", qualifiedByName = "franchiseToId"),
			@Mapping(target = "characters", source = "characters", qualifiedByName = "charactersToIds")
	})
	public abstract MovieDTO movieToMovieDto(Movie movie);
	
	public abstract Collection<MovieDTO> movieToMovieDto(Collection<Movie> movies);
	
	@Mappings({
			@Mapping(target = "franchise", source = "franchiseId", qualifiedByName = "franchiseIdToFranchise"),
			@Mapping(target = "characters", source = "characters", qualifiedByName = "characterIdsToCharacters")
	})
	public abstract Movie movieDtoToMovie(MovieDTO dto);
	
	
	@Mapping(target = "characters", source = "characters", qualifiedByName = "charactersToIds")
	public abstract MovieByFranchiseDTO movieToMovieByFranchiseDto(Movie movie);
	
	public abstract Collection<MovieByFranchiseDTO> movieToMovieByFranchiseDto(Collection<Movie> movie);
	
	public abstract Movie movieAddDtoToMovie(AddMovieDTO dto);
	
	@Named("franchiseToId")
	Integer mapFranchiseToId(Franchise franchise) {
		if (franchise == null) return null;
		return franchise.getId();
	}
	
	@Named("franchiseIdToFranchise")
	Franchise mapIdToFranchise(Integer id) {
		if (id == null) return null;
		return franchiseService.findById(id);
	}
	
	@Named("charactersToIds")
	Set<Integer> mapCharactersToIds(Collection<Character> characters) {
		if (characters == null) return null;
		return characters.stream().map(Character::getId).collect(Collectors.toSet());
	}
	
	@Named("characterIdsToCharacters")
	Set<Character> mapIdsToCharacters(Collection<Integer> ids) {
		if (ids == null) return null;
		return ids.stream().map(id -> characterService.findById(id)).collect(Collectors.toSet());
	}
}
