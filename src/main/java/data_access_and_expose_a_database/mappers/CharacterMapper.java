package data_access_and_expose_a_database.mappers;

import data_access_and_expose_a_database.models.Character;
import data_access_and_expose_a_database.models.Movie;
import data_access_and_expose_a_database.models.dto.character.AddCharacterDTO;
import data_access_and_expose_a_database.models.dto.character.CharacterByFranchiseDTO;
import data_access_and_expose_a_database.models.dto.character.CharacterByMovieDTO;
import data_access_and_expose_a_database.models.dto.character.CharacterDTO;
import data_access_and_expose_a_database.services.movie.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring",
		uses = {
		MovieService.class
})
public abstract class CharacterMapper {
	@Autowired
	protected MovieService movieService;
	
	@Mapping(target = "movies", source = "movies", qualifiedByName = "moviesToIds")
	public abstract CharacterDTO characterToCharacterDto(Character character);
	
	public abstract Collection<CharacterDTO> characterToCharacterDto(Collection<Character> characters);
	
	@Mapping(target = "movies", source = "movies", qualifiedByName = "movieIdsToMovies")
	public abstract Character characterDtoToCharacter(CharacterDTO dto);
	
	public abstract Collection<Character> characterDtoToCharacter(Collection<CharacterDTO> dtos);
	
	public abstract CharacterByMovieDTO characterToCharacterByMovieDto(Character character);
	public abstract Collection<CharacterByMovieDTO> characterToCharacterByMovieDto(Collection<Character> characters);
	
	@Mapping(target = "movies", source = "movies", qualifiedByName = "moviesToNames")
	public abstract CharacterByFranchiseDTO characterToCharacterByFranchiseDto(Character character);
	public abstract Collection<CharacterByFranchiseDTO> characterToCharacterByFranchiseDto(Collection<Character> character);
	
	public abstract Character addDtoToCharacter(AddCharacterDTO dto);
	
	@Named("movieIdsToMovies")
	Set<Movie> mapIdsToMovies(Set<Integer> source) {
		if (source == null) return null;
		
		return source.stream().map(id -> movieService.findById(id)).collect(Collectors.toSet());
	}
	
	@Named("moviesToIds")
	Set<Integer> mapMoviesToIds(Set<Movie> source) {
		if (source == null) return null;
		
		return source.stream().map(Movie::getId).collect(Collectors.toSet());
	}
	
	@Named("moviesToNames")
	Set<String> mapMoviesToMovieNames(Set<Movie> source) {
		if (source == null) return null;
		
		return source.stream().map(Movie::getTitle).collect(Collectors.toSet());
	}
}
