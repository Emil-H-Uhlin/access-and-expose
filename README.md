## About 
Project created with Java using Spring and RESTful api to interact with generated database (with hibernate).

### Implemented functionality
- DTOs wrapping incoming and outgoing data
- 'Skinny controllers' i.e. business logic separated into services and mapping separated into mappers.
- Full CRUD for major entities:
   - Character
   - Movie
   - Franchise
- Endpoints for reading data as well as related data of items

- Endpoints for:
   - Getting all _movies_ in a given _franchise_
   - Getting all the _characters_ in a given _movie_
   - Getting all the _characters_ in a _franchise_

- Swagger/OpenAPI documentation
- Data seeding via Spring with _data.sql_

## Installation
Create the project by: 
With intelliJ: 
- file -> new -> project from Version Control - paste _https://gitlab.com/Emil-H-Uhlin/access-and-expose.git_.

## Run
It is not deployed anywhere, but you can run it locally using a local Postgres instance:
- Create an empty database '**_moviefranchisedb_**' (or with other name if you prefer, but you'll have to change the _application.properties_ url)
- Enter username and password to access local instance
- If you'd like to seed additional data add queries into _data.sql_


Run the program and go to **_localhost:8080/swagger-ui.html_** to view available queries and try them out.
You can also query by visiting _localhost:8080/api/v1/characters_ for example, in your browser.
